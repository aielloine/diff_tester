# diff_tester
A web application that will test the differences of a website.


## Please install first:
```
composer require "jonnyw/php-phantomjs:4.*"
sudo apt-get install php-imagick
```
Create a data base, and configure your application in the config.cnf file.

## Some dependies:
- [php phantomjs](http://jonnnnyw.github.io/php-phantomjs/)
- php-imagick

## Some screens :
![Screen of diff_tester](AppPictures/1.png?raw=true "list of difs")
![Screen of diff_tester](AppPictures/2.png?raw=true "show difs")
